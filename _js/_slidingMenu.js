(function($){
	
	$.fn.slidingMenu = function(options) {
		
		options = $.extend({
			overlap : 10,
			speed : 500,
			reset : 2500,
			color : '#0b2b61',
			easing : 'swing'			
		}, options);
		
		return this.each(function() {
			
			var menu = $(this);
			var selectedItem = $(".current", menu);
			var slider;
			
			selectedItem.animate({
				color: '#196199'							
			}, 400);
			
			//Add the slider
			$("<li id='slider'></li>").css({
				width : selectedItem.outerWidth(),
				height : selectedItem.outerHeight() + options.overlap,
				left : selectedItem.position().left,
				top : selectedItem.position().top - options.overlap/2
			}).appendTo(this);
			
			slider = $("#slider", menu);
			
			// click handlers
			menu.delegate("li:not(.current)", "click", function() {
				
				$this = $(this);
				var current = $(".current", menu);
				console.log("Clicked");
				current.removeClass('current');
				$this.addClass('current');
				
				slider.animate({
					left : $(this).position().left,
					width : $(this).width()
				}, {
					duration : options.speed,
					easing : options.easing,
					queue : false 
				});
				
				current.animate({
					color: 'white'							
				}, 400);
				
				$this.animate({
					color: '#196199'						
				}, 400);
				
				
				var calBar = "#" + $(this).attr("id") + "SectionCalendarBar";				
				var calBarCur = "#" + $(current).attr("id") + "SectionCalendarBar";
				
				var techBar = "#" + $(this).attr("id") + "SectionTechBar";				
				var techBarCur = "#" + $(current).attr("id") + "SectionTechBar";				
								
				$(calBar).show();
				$(calBarCur).hide();
				
				$(techBar).show();
				$(techBarCur).hide();		
				
				
			});
			
		});
		
		
		
	}
})(jQuery);
