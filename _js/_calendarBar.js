(function($) {

	var monthMap = {
		Jan : '1',
		Feb : '2',
		Mar : '3',
		Apr : '4',
		May : '5',
		Jun : '6',
		Jul : '7',
		Aug : '8',
		Sep : '9',
		Oct : '10',
		Nov : '11',
		Dec : '12'
	}

	var defaultOptions = {

	};

	var methods = {
		init : function(options) {

			var years = 10;

			var i;

			var start = options.start;
			var end = options.start;

			for ( i = 0; i < years; i++) {
				var yearBlock = $('<div class="cYearBlock">' + start + '<div>');
				yearBlock.appendTo(this);
				start++;
			}

		},

		addTime : function(period, scrollT, section) {

			var splitStartPeriod = period.start.split(' ');

			var splitEndPeriod = period.end.split(' ');
			var months = period.months;

			if (period.end === "Present") {
				splitEndPeriod = "Apr 2013".split(' ');
			}

			var sMonth = splitStartPeriod[0];
			var sYear = splitStartPeriod[1];

			var eMonth = splitEndPeriod[0];
			var eYear = splitEndPeriod[1];

			var sMonFill = monthMap[sMonth] * 9;
			var sYearFill = (sYear - 2004) * 108;

			var totalFill = sMonFill + sYearFill;
			var monthsFill = months * 9;
			
			
			
			
			var sectionSelector = '#' + section;
			
			var currentInfoBox = $(sectionSelector).children().last();
			
						
			var iYearB = $('<div class="calInfoBlock"></div>');

			iYearB.css({
				left : totalFill,
				width : monthsFill
			});

			iYearB.click(function() {
				console.log($(this).width());
				$('html, body').animate({
					scrollTop : scrollT
				}, 1000);
				
				currentInfoBox
				
				console.log($('.infoBox')[0]);
				
				currentInfoBox.stop().delay(500).animate({backgroundColor: '#CCC'}, 100).animate({backgroundColor:'white'}, 1000);
				
				
				
			});

			iYearB.appendTo(this);

		}
	}

	$.fn.calendarBar = function(method) {

		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if ( typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Method ' + method + ' does not exist on calendarBar');
		}

	};
})(jQuery);
