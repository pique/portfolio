(function($) {

	var defaultOptions = {

	};

	var methods = {
		init : function(options) {

		},

		addInfo : function() {

			var section = this.attr('id');
			var dataSection = infoBlob[section];
			
			var techItems = techBlob[section];
			var source = $("#techbar-template").html();
			var template = Handlebars.compile(source);
			var html = template(techItems);
			var workItem = $($.parseHTML(html))
			var $techbar = $("#" + section + "TechBar");
			(workItem).appendTo($techbar);
		
			var calOptions = {
				start: "2004",
				end: "2013"
			}
			
			var calendarBar = "#" + section + "CalendarBar";			
			$(calendarBar).calendarBar('init', calOptions);

			var pointer = 0;
			var source = $("#infoBox-template").html();
			Handlebars.registerPartial("logo", $("#logo-template").html());
			Handlebars.registerPartial("info", $("#info-template").html());
			var template = Handlebars.compile(source);
			
			for ( pointer = 0; pointer < dataSection.length; pointer++) {
				var html = template(dataSection[pointer]);
				var workItem = $($.parseHTML(html))
				
				var scrollT = this.height();
				workItem.appendTo(this);
				console.log( section + " -- " + scrollT)
				$(calendarBar).calendarBar('addTime', dataSection[pointer].period, scrollT, section);

			}

		}
	}

	$.fn.infoBox = function(method) {

		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if ( typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Method ' + method + ' does not exist on jQuery.tooltip');
		}

	};
})(jQuery);
