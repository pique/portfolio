infoBlob = {
	workExperienceSection : [{
		period : {
			start : 'Aug 2012',
			end : 'Present',
			months : '10'
		},
		where : {
			name : 'Amazon.com',
			logo : 'amazon1.png',
			role : 'Software developer'
		},
		info : [{
			title : '',
			description : new Handlebars.SafeString('<b>Developer Portal</b>, Developed the frontend for game' + 'analytics under the amazon kindle developer portals hood. This website graphs various user engagement data which ' + 'helps the game developers to analyze their game. This is expected to go public by Feb 2013. Backend data is processed using Hadoop, Hive and DynamoDB.</br></br>' + '<b>Amazon Insights </b>(2013, Google Web Toolkit, Guice, MVP, Mockito, CSS, jQuery, Java, Spring, Highcharts). Developing a website from scratch to provide cross-platform analytics for mobile game developers.')
		}]
	}, {
		period : {
			start : 'Aug 2011',
			end : 'Jun 2012',
			months : '12'
		},
		where : {
			name : 'Amazon.com',
			logo : 'Juniper1.png',
			role : 'Software developer'
		},
		info : [{
			title : '',
			description : new Handlebars.SafeString('<b>Developer Portal</b>, Developed the frontend for game' + 'analytics under the amazon kindle developer portals hood. This website graphs various user engagement data which ' + 'helps the game developers to analyze their game. This is expected to go public by Feb 2013. Backend data is processed using Hadoop, Hive and DynamoDB.</br></br>' + '<b>Amazon Insights </b>(2013, Google Web Toolkit, Guice, MVP, Mockito, CSS, jQuery, Java, Spring, Highcharts). Developing a website from scratch to provide cross-platform analytics for mobile game developers.')
		}]
	}, {
		period : {
			start : 'May 2011',
			end : 'Jul 2011',
			months : '3'
		},
		where : {
			name : 'Amazon.com',
			logo : 'Ncsu.png',
			role : 'Software developer'
		},
		info : [{
			title : '',
			description : new Handlebars.SafeString('<b>Developer Portal</b>, Developed the frontend for game' + 'analytics under the amazon kindle developer portals hood. This website graphs various user engagement data which ' + 'helps the game developers to analyze their game. This is expected to go public by Feb 2013. Backend data is processed using Hadoop, Hive and DynamoDB.</br></br>' + '<b>Amazon Insights </b>(2013, Google Web Toolkit, Guice, MVP, Mockito, CSS, jQuery, Java, Spring, Highcharts). Developing a website from scratch to provide cross-platform analytics for mobile game developers.')
		}]
	}],
	educationSection : []
};

techBlob = {
	workExperienceSection : {
		tech : ["HTML", "CSS", "Javascript", "jQuery", "Selenium", "GWT", "Spring", "Handlebars.js", "Java", "AWS", "C"]
	},
	educationSection : {
		tech : ["Ruby", "Rails", "HTML", "CSS", "Javascript", "jQuery"]
	}
}

